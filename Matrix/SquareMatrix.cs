using System;
using ComputationMath.Vector;
using ComputationMath.MatrixAlgorithm.Factorization;

namespace ComputationMath.Matrix
{
    public class SquareMatrix: RectangleMatrix
    {
        public SquareMatrix() {}
        public SquareMatrix(double[][] matrix)
            => SetMatrix(matrix);

        public SquareMatrix(int size)
            :base(size, size)
        {}

        public SquareMatrix(IMatrix matrix)
            :base(matrix.ToArray()) 
        {}

        public virtual int Size { get => Rows; }
        public override int Cols { get => Rows; }

        public override void SetMatrix(double[][] array)
        {
            int cols = array[0].Length;
            if (cols != array.Length)
            {
                throw new ArgumentException();
            }
            foreach(double[] row in array)
            {
                if (row.Length != cols)
                {
                    throw new ArgumentException();
                }
            }
            matrix = array;
        }

        public static SquareMatrix operator * (double multiplier, SquareMatrix matrix)
			=> new SquareMatrix(matrix.Mul(multiplier).ToArray());
		public static SquareMatrix operator * (SquareMatrix matrix, double multiplier)
			=> new SquareMatrix(matrix.Mul(multiplier).ToArray());
		public static SquareMatrix operator * (SquareMatrix matrix0, IMatrix matrix1)
			=> new SquareMatrix(matrix0.Mul(matrix1).ToArray());
        public static ColumnVector operator * (SquareMatrix matrix, ColumnVector vector)
			=> new ColumnVector(matrix.Mul(vector));
        // public static SquareMatrix operator * (SquareMatrix matrix, RowVector vector)
		// 	=> new SquareMatrix(matrix.Mul(vector).ToArray());
		public static SquareMatrix operator - (SquareMatrix matrix1, SquareMatrix matrix2)
			=> new SquareMatrix(matrix1.Sub(matrix2).ToArray());
		public static SquareMatrix operator + (SquareMatrix matrix1, SquareMatrix matrix2)
			=> new SquareMatrix(matrix1.Sum(matrix2).ToArray());


        public new SquareMatrix Transpose()
        {
            return new SquareMatrix(new RectangleMatrix(ToArray()).Transpose());
        }

        protected override RectangleMatrix TransposeImplementation()
        {
            return Transpose();
        }


        public virtual bool IsDiagonallyDominant()
        {
            for (int i = 0; i < Size; i++)
            {
                double sum = 0;
                if(Get(i, i) < 0)
                {
                    return false;
                }
                foreach(double x in GetRow(i))
                {
                    sum += Math.Abs(x);
                }
                if (sum - Get(i, i) > Get(i, i))
                {
                    return false;
                }
            }
            return true;
        }

        public virtual bool DiagonalIsPositive()
        {
            for (int i = 0; i < Size; i++)
            {
                if (Get(i, i) < 0)
                {
                    return false;
                }
            }
            return true;
        }

        // TODO: check whether the method is safe
        public double Determinant()
        {
            return new LDUFactorization(this).GetDeterminant();
        }

        // TODO: check whether the method is safe
        public SquareMatrix InverseMatrix()
        {
            return new LUFactorization(this).InverseMatrix();
        }
    }
}