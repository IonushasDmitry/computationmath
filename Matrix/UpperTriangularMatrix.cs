using System;
using ComputationMath.Matrix;
using ComputationMath.Vector;

namespace ComputationMath.Matrix
{
	public class UpperTriangularMatrix: SquareMatrix
	{
		public UpperTriangularMatrix(SquareMatrix squareMatrix)
		{
			int size = squareMatrix.Size;
			matrix = new double[size][];
			for (int i = 0; i < size; i++)
			{
				for (int j = 0; j < i; j++)
				{
					if (squareMatrix.Get(i, j) != 0)
					{
						throw new ArgumentException("Elements below main diagonal can not be zero");
					}
				}
				matrix[i] = new double[size-i];
				for (int j = i; j < size; j++)
				{
					Set(i, j, squareMatrix.Get(i, j));
				}
			}
		}

		public UpperTriangularMatrix(double[][] matrix)
			:this(new SquareMatrix(matrix))
		{}

		public UpperTriangularMatrix(IMatrix matrix)
			:this(new SquareMatrix(matrix))
		{}

		public UpperTriangularMatrix(RectangleMatrix matrix)
			:this(new SquareMatrix(matrix))
		{}

		public UpperTriangularMatrix(int size)
		{
			matrix = new double[size][];
			for (int i = 0; i < size; i++)
			{
				matrix[i] = new double[size-i];
			}
		}

		public override double Get(int row, int col)
		{
			if (row > col)
			{
				return 0;
			}
			return matrix[row][col - row];
		}

		public override void Set(int row, int col, double value)
		{
			if (row > col)
			{
				if (value != 0)
				{
					throw new ArgumentException("Elements below main diagonal can not be zero");
				}
				return;
			}
			matrix[row][col - row] = value;
		}


		public new LowerTriangularMatrix Transpose()
        {
            return new LowerTriangularMatrix(new SquareMatrix(ToArray()).Transpose());
        }

        protected override RectangleMatrix TransposeImplementation()
        {
            return Transpose();
        }


		public ColumnVector Solve(ColumnVector f)
		{
			if (Size != f.Size)
			{
				throw new ArgumentException("Size of vector does not match size of matrix");
			}
			double[] x = new double[Size];
			for (int i = Size-1; i >= 0; i--)
			{
				double sum = f.Get(i);
				for (int j = i+1; j < Size; j++)
				{
					sum -= x[j] * Get(i, j);
				}
				x[i] = sum / Get(i, i);
			}

			return new ColumnVector(x);
		}
	}
}