using System;

namespace ComputationMath.Matrix {
    public class DiagonalMatrix : IMatrix {
        protected double[] mainDiagonalVector;

        public DiagonalMatrix (double[] matrix) {
            this.mainDiagonalVector = matrix;
        }

        public DiagonalMatrix (double[][] matrix) 
            : this (new SquareMatrix (matrix)) { }

        public DiagonalMatrix (SquareMatrix matrix) {
            int size = matrix.Rows;
            mainDiagonalVector = new double[size];
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    if (i != j && matrix.Get (i, j) != 0) {
                        throw new ArgumentException ();
                    }
                }
                mainDiagonalVector[i] = matrix.Get (i, i);
            }
        }

        public double Get (int row, int col) {
            if (row != col) {
                return 0;
            }
            return mainDiagonalVector[row];
        }

        public void Set(int row, int col, double value)
        {
            if (row != col)
            {
                throw new ArgumentException("Non diagonal emenents can not be any number except 0");
            }
            mainDiagonalVector[row] = value;
        }

        public int Size { get => mainDiagonalVector.Length; }
        public int Cols { get => Size; }
        public int Rows { get => Size; }

        public double[] GetRow (int row) {
            double[] vector = new double[Cols];
            vector[row] = mainDiagonalVector[row];
            return vector;
        }

        public double[] GetCol (int col) {
            double[] vector = new double[Rows];
            vector[col] = mainDiagonalVector[col];
            return vector;
        }

        public DiagonalMatrix Sum (DiagonalMatrix diagonalMatrix) {
            if (Rows != diagonalMatrix.Rows) {
                throw new ArgumentException ();
            }

            double[] vector = new double[Rows];
            for (int i = 0; i < Rows; i++) {
                vector[i] = mainDiagonalVector[i] + diagonalMatrix.Get (i, i);
            }
            return new DiagonalMatrix (vector);
        }

        public DiagonalMatrix Sub (DiagonalMatrix diagonalMatrix) {
            return this.Sum (diagonalMatrix.Mul (-1));
        }

        public DiagonalMatrix Mul (double multiplier) {
            double[] vector = new double[Rows];
            for (int i = 0; i < Rows; i++) {
                vector[i] = mainDiagonalVector[i] * multiplier;
            }
            return new DiagonalMatrix (vector);
        }

        public DiagonalMatrix Div (double divider) {
            return this.Mul (1.0 / divider);
        }

        public IMatrix Clone () {
            return new DiagonalMatrix ((double[]) mainDiagonalVector.Clone ());
        }

        public double[][] ToArray()
        {
            double[][] array = new double[Size][];
            for (int i = 0; i < Size; i++)
            {
                array[i] = new double[Size];
                array[i][i] = Get(i, i);
            }
            return array;
        }
    }
}