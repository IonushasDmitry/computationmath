using System;
using ComputationMath.Matrix;

namespace ComputationMath.Matrix
{
    public class SymmetricMatrix: SquareMatrix
    {
        public SymmetricMatrix() {}

        public SymmetricMatrix(double[][] matrix)
        {
            if (!isSymmetric(matrix))
            {
                throw new ArgumentException("Matrix isn't symmetric");
            }
            int size = matrix.Length;
            this.matrix = new double[size][];
            for (int i = 0; i < size; i++)
            {
                this.matrix[i] = new double[size - i];
                for (int j = i; j < size; j++)
                {
                    Set(i, j, matrix[i][j]);
                }
            }
        }

        public SymmetricMatrix(IMatrix matrix)
            :this(matrix.ToArray())
        {}

        public override int Rows { get => matrix.Length; }
        public override int Cols { get => Rows; }
        public override int Size { get => Rows; }

        public override double Get(int row, int col)
        {
            if (col >= row)
            {
                return matrix[row][col-row];
            }
            return matrix[col][row-col];
        }

        public override void Set(int row, int col, double value)
        {
            if (col >= row)
            {
                matrix[row][col-row] = value;
            }
            else
            {
                matrix[col][row-col] = value;
            }
        }

        public static bool isSymmetric(double[][] matrix)
        {
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    if (matrix[i][j] != matrix[j][i])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override double[][] ToArray()
        {
            double[][] array = new double[Size][];
            for (int i = 0; i < Size; i++)
            {
                array[i] = new double[Size];
                for (int j = 0; j < Size; j++)
                {
                    array[i][j] = Get(i, j);
                }
            }
            return array;
        }
    }
}