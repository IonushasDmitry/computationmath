using System;

namespace ComputationMath.Matrix
{
    public abstract class AbstractMatrix: IMatrix
    {
        protected double[][] matrix;
		public virtual int Cols { get => matrix[0].Length; }
		public virtual int Rows { get => matrix.Length; }	
        public virtual double Get(int row, int col) => matrix[row][col];

        public virtual double[] GetRow(int rowNumber)
		{
			double[] row = new double[Cols];
			for (int i = 0; i < Cols; i++)
			{
				row[i] = this.Get(rowNumber, i);
			}
			return (double[])row.Clone();
		}
		public virtual double[] GetCol(int colNumber)
		{
			double[] col = new double[Rows];
			for (int i = 0; i < this.Rows; i++)
			{
				col[i] = this.Get(i, colNumber);
			}
			return (double[])col.Clone();
		}

        public virtual IMatrix Clone()
		{
			return (IMatrix)this.MemberwiseClone();
		}

		// public virtual double[][] ToArray()
		// {
		// 	return matrix;
		// }

		public virtual double[][] ToArray()
		{
			double[][] array = new double[Rows][];
			for (int i = 0; i < Rows; i++)
			{
				array[i] = new double[Cols];
				for (int j = 0; j < Cols; j++)
				{
					array[i][j] = Get(i, j);
				}
			}
			return array;
		}

		public override string ToString()
		{
			string str = "";
			for (int i = 0; i < Rows; i++)
			{
				for (int j = 0; j < Cols; j++)
				{
					str += Math.Round(Get(i, j), 3, MidpointRounding.AwayFromZero).ToString() + " ";
				}
				str += "\n";
			}
			// foreach(double[] row in matrix)
			// {
			// 	foreach(double x in row)
			// 	{
			// 		str += Math.Round(x, 3, MidpointRounding.AwayFromZero).ToString() + " ";
			// 	}
			// 	str += "\n";
			// }
			

			return str;
		}
    }
}