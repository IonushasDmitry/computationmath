using System;

namespace ComputationMath.Matrix
{
    public class SwapMatrix: SquareMatrix
    {
        private int[] indexes;

        public SwapMatrix(int size)
        {
            indexes = new int[size];
            for (int i = 0; i < size; i++)
            {
                indexes[i] = i;
            }
        }

        // TODO: check
        public SwapMatrix(SquareMatrix matrix)
        {
            int size = matrix.Size;
            indexes = new int[size];
            for (int i = 0; i < size; i++)
            {
                int counter = 0;
                for (int j = 0; j < size; j++)
                {
                    if (matrix.Get(i, i) != 0 && matrix.Get(i, j) != 1)
                    {
                        throw new ArgumentException();
                    }
                    if (matrix.Get(i, j) == 1)
                    {
                        counter++;
                        indexes[i] = j;
                    }
                }
                if (counter != 1)
                {
                    throw new ArgumentException();
                }
            }
        }

        public override int Size { get => Rows; }
        public override int Rows { get => indexes.Length; }
        public override int Cols { get => Rows; }

        public override void SwapRows(int row1, int row2)
        {
            if (row1 < 0 || row1 >= Size || row2 < 0 || row2 >= Size)
            {
                throw new ArgumentException();
            }
            int tmp = indexes[row1];
            indexes[row1] = indexes[row2];
            indexes[row2] = tmp;
        }

        public override double Get(int row, int col)
        {
            if (indexes[row] == col)
            {
                return 1;
            }
            return 0;
        }

        public override void Set(int row, int col, double value)
        {
            throw new NotSupportedException();
        }

        public SquareMatrix ToSquareMatrix()
        {
            return new SquareMatrix(ToArray());
        }

        public override double[][] ToArray()
        {
            double[][] matrix = new double[Size][];
            for (int i = 0; i < Size; i++)
            {
                matrix[i] = new double[Size];
                matrix[i][indexes[i]] = 1;
            }
            return matrix;
        }

        // public override ToString()
        // {

        // }
    }
}