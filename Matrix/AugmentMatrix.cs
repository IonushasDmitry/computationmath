using System;
using ComputationMath.Vector;

namespace ComputationMath.Matrix
{
    public class AugmentMatrix
    {
        protected RectangleMatrix mainMatrix;
        protected RectangleMatrix additionalMatrix;


        public AugmentMatrix(RectangleMatrix main, RectangleMatrix additional)
        {
            if (main.Rows != additional.Rows)
            {
                throw new ArgumentException("Number of rows of "
                    + "main and additional matrixes must match");
            }
            mainMatrix = main;
            additionalMatrix = additional;
        }
        public AugmentMatrix(RectangleMatrix main, IVector additional)
        {
            if (main.Rows != additional.Rows)
            {
                throw new ArgumentException("Number of rows of "
                    + "main and additional matrixes must match");
            }
            mainMatrix = main;
            additionalMatrix = additional.ToMatrix();
        }

        public RectangleMatrix MainMatrix { get => mainMatrix; }
        public RectangleMatrix AdditionalMatrix { get => additionalMatrix; }
        public int Rows { get => mainMatrix.Rows; }
        public int MainMatrixCols { get => mainMatrix.Cols; }
        public int AdditionalMatrixCols { get => additionalMatrix.Cols; }


        public double Get(int row, int col)
        {
            if (MainMatrixCols <= col)
            {
                return additionalMatrix.Get(row, col - MainMatrixCols);
            }
            return mainMatrix.Get(row, col);
        }

        public void Set(int row, int col, double value)
        {
            if (MainMatrixCols <= col)
            {
                additionalMatrix.Set(row, col - MainMatrixCols, value);
            }
            mainMatrix.Set(row, col, value);
        }

        public void SwapRows(int row1, int row2)
        {
            mainMatrix.SwapRows(row1, row2);
            additionalMatrix.SwapRows(row1, row2);
        }

        public void SwapMainMatrixCols(int col1, int col2)
        {
            mainMatrix.SwapCols(col1, col2);
        }
        public void SwapAdditionalMatrixCols(int col1, int col2)
        {
            additionalMatrix.SwapCols(col1, col2);
        }

        //TODO: do SwapCols should work so?
        // public void SwapBothMatrixCols(int col1, int col2)
        // {
        //     mainMatrix.SwapCols(col1, col2);
        //     additionalMatrix.SwapCols(col1, col2);
        // }


        public void MultiplyRow(int row, double multiplier)
        {
            mainMatrix.MultiplyRow(row, multiplier);
            additionalMatrix.MultiplyRow(row, multiplier);
        }

        public void DivideRow(int row, double divider)
        {
            mainMatrix.DivideRow(row, divider);
            additionalMatrix.DivideRow(row, divider);
        }

        public void AddToRow(int row, double[] mainVector, double[] additionalVector)
        {
            mainMatrix.AddToRow(row, mainVector);
            additionalMatrix.AddToRow(row, additionalVector);
        }

        public void AddToRow(int row, RowVector vector1, RowVector vector2)
        {
            mainMatrix.AddToRow(row, vector1);
            additionalMatrix.AddToRow(row, vector2);
        }

        public void DeleteRow(int row)
        {
            mainMatrix.DeleteRow(row);
            additionalMatrix.DeleteRow(row);
        }

        public void AddToRowMultipliedRow(int rowToMultiply, int rowToAdd, double multiplier)
        {
            mainMatrix.AddToRowMultipliedRow(rowToMultiply, rowToAdd, multiplier);
            additionalMatrix.AddToRowMultipliedRow(rowToMultiply, rowToAdd, multiplier);
        }

        public void MulMainMatrix(double multiplier)
        {
            mainMatrix *= multiplier;
        }

        public void MulAdditionalMatrix(double multiplier)
        {
            additionalMatrix *= multiplier;
        }

        public void MulMainMatrix(IMatrix multiplierMatrix)
        {
            mainMatrix *= multiplierMatrix;
        }

        public void MulAdditionalMatrix(IMatrix multiplierMatrix)
        {
            additionalMatrix *= multiplierMatrix;
        }
    }
}