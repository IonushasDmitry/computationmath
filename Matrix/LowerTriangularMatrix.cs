using System;
using ComputationMath.Vector;

namespace ComputationMath.Matrix
{
	public class LowerTriangularMatrix: SquareMatrix
	{
		public LowerTriangularMatrix(SquareMatrix squareMatrix)
		{
			int size = squareMatrix.Size;
			matrix = new double[size][];
			for (int i = 0; i < size; i++)
			{
				for (int j = i+1; j < size; j++)
				{
					if (squareMatrix.Get(i, j) != 0)
					{
						throw new ArgumentException("Elements above main diagonal can not be zero");
					}
				}
				matrix[i] = new double[i+1];
				for (int j = 0; j <= i; j++)
				{
					Set(i, j, squareMatrix.Get(i, j));
				}
			}
		}

		public LowerTriangularMatrix(double[][] matrix)
			:this(new SquareMatrix(matrix))
		{}

		public LowerTriangularMatrix(IMatrix matrix)
			:this(new SquareMatrix(matrix))
		{}

		public LowerTriangularMatrix(RectangleMatrix matrix)
			:this(new SquareMatrix(matrix))
		{}

		public LowerTriangularMatrix(int size)
		{
			matrix = new double[size][];
			for (int i = 0; i < size; i++)
			{
				matrix[i] = new double[i+1];
			}
		}

		public override double Get(int row, int col)
		{
			if (col > row)
			{
				return 0;
			}
			return matrix[row][col];
		}

		public override void Set(int row, int col, double value)
		{
			if (col > row)
			{
				if (value != 0)
				{
					throw new ArgumentException("Elements above main diagonal can not be zero");
				}
				return;
			}
			matrix[row][col] = value;
		}


		public new UpperTriangularMatrix Transpose()
        {
            return new UpperTriangularMatrix(new SquareMatrix(ToArray()).Transpose());
        }

        protected override RectangleMatrix TransposeImplementation()
        {
            return Transpose();
        }

		public ColumnVector Solve(ColumnVector f)
		{
			if (Size != f.Size)
			{
				throw new ArgumentException("Size of vector does not match size of matrix");
			}
			double[] x = new double[Size];
			for (int i = 0; i < Size; i++)
			{
				double sum = f.Get(i);
				for (int j = 0; j < i; j++)
				{
					sum -= x[j] * Get(i, j);
				}
				x[i] = sum / Get(i, i);
			}

			return new ColumnVector(x);
		}
	}
}