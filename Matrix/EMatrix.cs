namespace ComputationMath.Matrix
{
    public sealed class EMatrix: IMatrix
    {
        private int size;
        
        public EMatrix(int size)
        {
            this.size = size;
        }

        public double Get(int row, int col)
        {
            if (row == col)
            {
                return 1;
            }
            return 0;
        }
        public int Size { get => size; }
        public int Cols { get => size; }
        public int Rows { get => size; }
        public double[] GetRow(int row)
        {
            double[] rowVector = new double[size];
            rowVector[row] = 1;
            return rowVector;
        }
        public double[] GetCol(int col)
        {
            double[] colVector = new double[size];
            colVector[col] = 1;
            return colVector;
        }

        public double[][] ToArray()
        {
            double[][] matrixArray = new double[size][];
            for (int i = 0; i < size; i++)
            {
                matrixArray[i] = new double[size];
                matrixArray[i][i] = 1;
            }
            return matrixArray;
        }

        public SquareMatrix ToMatrix()
        {
            return new SquareMatrix(ToArray());
        }

        public IMatrix Clone()
        {
            return ToMatrix();
        }
    }
}