using System;

namespace ComputationMath.Matrix
{
    public class ThreeDiagonalMatrix: SquareMatrix
    {
        public ThreeDiagonalMatrix(SquareMatrix passedMatrix)
        {
            int size = passedMatrix.Size;
            matrix = new double[size][];
            for (int i = 0; i < size; i++)
            {
                matrix[i] = new double[3];
            }

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (Math.Abs(j - i) > 1 && passedMatrix.Get(i, j) != 0)
                    {
                        throw new ArgumentException("Passed matrix is'n three diagonal");
                    }
                }
            }

            matrix[0][0] = 0;
            matrix[0][1] = passedMatrix.Get(0, 0);
            matrix[0][2] = passedMatrix.Get(0, 1);
            for (int i = 1; i < size-1; i++)
            {
                matrix[i][0] = passedMatrix.Get(i, i-1);
                matrix[i][1] = passedMatrix.Get(i, i);
                matrix[i][2] = passedMatrix.Get(i, i+1);
            }
            matrix[size-1][0] = passedMatrix.Get(size-1, size-2);
            matrix[size-1][1] = passedMatrix.Get(size-1, size-1);
            matrix[size-1][2] = 0;
        }

        public ThreeDiagonalMatrix(double[][] passedMatrix)
            :this(new SquareMatrix(passedMatrix))
        {}

        public ThreeDiagonalMatrix(IMatrix passedMatrix)
            :this(new SquareMatrix(passedMatrix))
        {}

        public override int Size { get => Rows; }
        public override int Rows { get => matrix.Length; }
        public override int Cols { get => Size; }

        public override void SetMatrix(double[][] array)
        {
            throw new NotImplementedException();
        }

        public override double Get(int row, int col)
        {
            if (col < 0 || row < 0 || col >= Size || row >= Size)
            {
                throw new ArgumentException("Invalid index");
            }
            if (Math.Abs(col - row) > 1)
            {
                return 0;
            }
            return matrix[row][1 + (col-row)];
        }

        public override void Set(int row, int col, double value)
        {
            throw new NotImplementedException();
        }
    }
}