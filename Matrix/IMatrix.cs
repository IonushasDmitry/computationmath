namespace ComputationMath.Matrix
{
	public interface IMatrix
	{
		double Get(int row, int col);
		int Cols { get; }
		int Rows { get; }

		double[] GetRow(int row);
		double[] GetCol(int col);
		IMatrix Clone();
		double[][] ToArray();
	}
}