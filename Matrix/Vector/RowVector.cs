using System;
using ComputationMath.Matrix;

namespace ComputationMath.Vector
{
	public class RowVector: AbstractVector
	{
		public RowVector(RectangleMatrix matrix)
		{
			if (matrix.Rows == 1)
			{
				this.matrix = matrix;
			} 
			else 
			{
				throw new ArgumentException();
			}
		}
		public RowVector(RowVector vector)
		{
			this.matrix = vector.ToMatrix();
		}
		public RowVector(double[] vector)
		{
			double[][] matrix = new double[1][];
			matrix[0] = (double[]) vector.Clone();
			this.matrix = new RectangleMatrix(matrix);
		}
		public RowVector(int size)
		{
			double[][] matrix = new double[1][];
			matrix[0] = new double[size];
			this.matrix = new RectangleMatrix(matrix);
		}


		public override double Get(int i)
		{
			return matrix.Get(0, i);
		}
		public override void Set(int index, double value)
		{
			matrix.Set(0, index, value);
		}

		public override int Size { get => Cols; }
		public override int Capacity()
		{
			return Cols;
		}

		public double[] ToArray()
		{
			return (double[])this.matrix.GetRow(0).Clone();
		}


		public ColumnVector Transposed()
		{
			return new ColumnVector(this.ToArray());
		}
		
		public ColumnVector T { get => new ColumnVector(this.ToArray()); }

		protected delegate double BinaryOperation(double x, double y);

		protected RowVector SumOrSub(RowVector vector, BinaryOperation operation)
		{
			if (!this.HasEqualSize(vector))
			{
				throw new ArgumentException();
			}

			double[][] matrix = new double[1][];
			matrix[0] = new double[Capacity()];


			for (int i = 0; i < Capacity(); i++)
			{
				matrix[0][i] = operation(this.Get(i), vector.Get(i));
			}

			return new RowVector(new RectangleMatrix(matrix));
		}

		public RowVector Sum(RowVector vector) => SumOrSub(vector, (x, y) => x+y);
		public RowVector Sub(RowVector vector) => SumOrSub(vector, (x, y) => x-y);


		public RowVector Mul(double multiplier)
		{
			double[] matrix = new double[this.Capacity()];
			for(int i = 0; i < this.Capacity(); i++)
			{
				matrix[i] = multiplier * this.Get(i);
			}
			return new RowVector(matrix);
		}
		public double Mul(ColumnVector vector)
		{
			if (this.Capacity() != vector.Capacity())
			{
				throw new ArgumentException();
			}

			double sum = 0;
			for (int i = 0; i < this.Capacity(); i++)
			{
				sum += this.Get(i) * vector.Get(i);
			}

			return sum;
		}

		public static RowVector operator * (double multiplier, RowVector vector)
			=> vector.Mul(multiplier);
		public static RowVector operator * (RowVector vector, double multiplier)
			=> vector.Mul(multiplier);
		public static double operator * (RowVector rowVector, ColumnVector columnVector)
			=> rowVector.Mul(columnVector);

		public bool IsProportional(RowVector vector)
		{
			if (Capacity() != vector.Capacity())
			{
				return false;
			}
			if (IsZeroVector() ^ vector.IsZeroVector())
			{
				return false;
			}

			double coefficient = 0;
			for (int i = 0; i < Capacity(); i++)
			{
				if (Get(i) != 0 && vector.Get(i) != 0)
				{
					coefficient = Get(i) / vector.Get(i);
					break;
				}
			}
			if (coefficient == 0)
			{
				return false;
			}

			for (int i = 0; i < Capacity(); i++)
			{
				if (Get(i) != coefficient*vector.Get(i))
				{
					return false;
				}
			}
			return true;
		}

		// public override string ToString()
		// {
		// 	string str = "";
		// 	for (int i = 0; i < this.Capacity(); i++)
		// 	{
		// 		str += this.Get(i).ToString() + ' ';
		// 	}
		// 	return str;
		// }
	}
}