using System;
using ComputationMath.Matrix;

namespace ComputationMath.Vector
{
	public class ColumnVector: AbstractVector
	{
		public ColumnVector(RectangleMatrix matrix)
		{
			if (matrix.Cols != 1)
			{
				throw new ArgumentException();
			}
			this.matrix = matrix;
		}
		public ColumnVector(ColumnVector vector)
		{
			this.matrix = vector.ToMatrix();
		}
		public ColumnVector(double[] vector)
		{
			double[][] matrix = new double[vector.Length][];
			for (int i = 0; i < vector.Length; i++)
			{
				matrix[i] = new double[1];
				matrix[i][0] = vector[i];
			}
			this.matrix = new RectangleMatrix(matrix);
		}
		public ColumnVector(int size)
		{
			double[][] matrix = new double[size][];
			for (int i = 0; i < size; i++)
			{
				matrix[i] = new double[1];
				matrix[i][0] = 0;
			}
			this.matrix = new RectangleMatrix(matrix);
		}


		public override double Get(int i)
		{
			return matrix.Get(i, 0);
		}
		public override void Set(int index, double value)
		{
			matrix.Set(index, 0, value);
		}

		public override int Size { get => Rows; }
		public override int Capacity()
		{
			return Rows;
		}

		public double[] ToArray()
		{
			return (double[])this.matrix.GetCol(0);
		}


		public RowVector Transposed()
		{
			return new RowVector(this.ToArray());
		}

		public RowVector T { get => new RowVector(this.ToArray()); }


		protected delegate double BinaryOperation(double x, double y);

		protected ColumnVector SumOrSub(ColumnVector vector, BinaryOperation operation)
		{
			if (!this.HasEqualSize(vector))
			{
				throw new ArgumentException();
			}

			double[][] matrix = new double[Capacity()][];
			for (int i = 0; i < Capacity(); i++)
			{
				matrix[i] = new double[1];
			}


			for (int i = 0; i < Capacity(); i++)
			{
				matrix[i][0] = operation(this.Get(i), vector.Get(i));
			}

			return new ColumnVector(new RectangleMatrix(matrix));
		}

		public ColumnVector Sum(ColumnVector vector) => SumOrSub(vector, (x, y) => x+y);
		public ColumnVector Sub(ColumnVector vector) => SumOrSub(vector, (x, y) => x-y);


		public ColumnVector Mul(double multiplier)
		{
			double[] matrix = new double[this.Capacity()];
			for(int i = 0; i < this.Capacity(); i++)
			{
				matrix[i] = multiplier * this.Get(i);
			}
			return new ColumnVector(matrix);
		}
		public RectangleMatrix Mul(RowVector rowVector)
		{
			if (this.Capacity() != rowVector.Capacity())
			{
				throw new ArgumentException();
			}

			double[][] matrix = new double[this.Capacity()][];
			for (int i = 0; i < this.Capacity(); i++)
			{
				matrix[i] = new double[rowVector.Capacity()];
			}

			for (int i = 0; i < this.Capacity(); i++)
			{
				for (int j = 0; j < this.Capacity(); j++)
				{
					matrix[i][j] = this.Get(i)*rowVector.Get(j);
				}
			}

			return new RectangleMatrix(matrix);
		}

		public static ColumnVector operator * (double multiplier, ColumnVector vector)
			=> vector.Mul(multiplier);
		public static ColumnVector operator * (ColumnVector vector, double multiplier)
			=> vector.Mul(multiplier);
		public static RectangleMatrix operator * (ColumnVector columnVector, RowVector rowVector)
			=> columnVector.Mul(rowVector);
		public static ColumnVector operator - (ColumnVector vector1, ColumnVector vector2)
			=> vector1.Sub(vector2);
		public static ColumnVector operator + (ColumnVector vector1, ColumnVector vector2)
			=> vector1.Sum(vector2);

		public bool IsProportional(ColumnVector vector)
		{
			if (Capacity() != vector.Capacity())
			{
				return false;
			}
			if (IsZeroVector() ^ vector.IsZeroVector())
			{
				return false;
			}

			double coefficient = 0;
			for (int i = 0; i < Capacity(); i++)
			{
				if (Get(i) != 0 && vector.Get(i) != 0)
				{
					coefficient = Get(i) / vector.Get(i);
					break;
				}
			}
			if (coefficient == 0)
			{
				return false;
			}

			for (int i = 0; i < Capacity(); i++)
			{
				if (Get(i) != coefficient*vector.Get(i))
				{
					return false;
				}
			}
			return true;
		}

		public override string ToString()
		{
			string str = "";
			for (int i = 0; i < this.Capacity()-1; i++)
			{
				str += Math.Round(this.Get(i), 3, MidpointRounding.AwayFromZero).ToString() + '\n';
			}
			str += Math.Round(this.Get(Capacity()-1), 3, MidpointRounding.AwayFromZero).ToString();
			return str;
		}
	}
}