using ComputationMath.Matrix;

namespace ComputationMath.Vector
{
	public interface IVector
	{
		double Get(int index);
		void Set(int index, double value);
		int Cols { get; }
		int Rows { get; }
		int Size { get; }
		int Capacity();

		// get copy of matrix
		RectangleMatrix ToMatrix();
		double GetMagnitude();
		// bool HasEqualSize(IVector vector);
	}
}