using System;
using ComputationMath.Matrix;

namespace ComputationMath.Vector
{
	public abstract class AbstractVector: IVector
	{
		protected RectangleMatrix matrix;

		protected double Get(int i, int j)
		{
			return matrix.Get(i, j);
		}
		public abstract double Get(int index);
		public abstract void Set(int index, double value);

		public int Cols { get => matrix.Cols; }
		public int Rows { get => matrix.Rows; }
		public abstract int Size { get; }
		
		public abstract int Capacity();
		
		public RectangleMatrix ToMatrix()
		{
			return (RectangleMatrix)this.matrix.Clone();
		}

		public double GetMagnitude()
		{
			double sumOfSquares = 0;
			for (int i = 0; i < this.Capacity(); i++)
			{
				sumOfSquares += Math.Pow(this.Get(i), 2);
			}

			return Math.Sqrt(sumOfSquares);
		}
		
		public bool IsZeroVector()
		{
			for (int i = 0; i < Capacity(); i++)
			{
				if (Get(i) != 0)
				{
					return false;
				}
			}
			return true;
		}

		protected bool HasEqualSize(IVector vector)
		{
			if (this.Cols == vector.Cols && this.Rows == vector.Rows)
			{
				return true;
			}
			return false;
		}

		public override string ToString()
		{
			string str = "";
			for (int i = 0; i < this.Size; i++)
			{
				str += Math.Round(this.Get(i), 3, MidpointRounding.AwayFromZero).ToString() + ' ';
			}
			return str;
		}
	}
}