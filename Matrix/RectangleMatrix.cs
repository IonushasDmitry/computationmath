using System;
using System.Collections.Generic;
using ComputationMath.Vector;

namespace ComputationMath.Matrix
{
	public class RectangleMatrix: AbstractMatrix
	{
		public RectangleMatrix() {}
		public RectangleMatrix(double[][] matrix)
		{
			for (int i = 1; i < matrix.Length; i++)
            {
                if (matrix[i].Length != matrix[0].Length)
                {
                    throw new ArgumentException();
                }
            }
			
			this.matrix = matrix;
		}
		public RectangleMatrix(int rows, int cols)
		{
			matrix = new double[rows][];
			for (int i = 0; i < rows; i++)
			{
				matrix[i] = new double[cols];
			}
		}


		public virtual void Set(int row, int col, double value)
		{
			if (row < 0 || row >= Rows || col < 0 || col >= Cols)
			{
				throw new ArgumentException();
			}
			matrix[row][col] = value;
		}

		public virtual void SetMatrix(double[][] array)
		{
			int cols = array[0].Length;
			foreach(double[] row in array)
			{
				if (row.Length != cols)
				{
					throw new ArgumentException();
				}
			}
			matrix = array;
		}

		protected delegate double BinaryOperation(double x, double y);
		protected RectangleMatrix SumOrSub(RectangleMatrix matrix, BinaryOperation operation)
		{
			if (this.Cols != matrix.Cols || this.Rows != matrix.Rows)
			{
				throw new ArgumentException();
			}

			double[][] array = new double[Rows][];
			for (int i = 0; i < Rows; i++)
			{
				array[i] = new double[Cols];
			}

			for (int i = 0; i < Rows; i++)
			{
				for (int j = 0; j < Cols; j++)
				{
					array[i][j] = operation(this.Get(i, j), matrix.Get(i, j));
				}
			}

			return new RectangleMatrix(array);
		}
		public virtual RectangleMatrix Sum(RectangleMatrix matrix) => SumOrSub(matrix, (x, y) => x+y);
		public virtual RectangleMatrix Sub(RectangleMatrix matrix) => SumOrSub(matrix, (x, y) => x-y);


		public virtual RectangleMatrix Mul(double multiplier)
		{
			double[][] matrix = new double[this.Rows][];
			for (int i = 0; i < this.Rows; i++)
			{
				matrix[i] = new double[this.Cols];
			}

			for (int i = 0; i < this.Rows; i++)
			{
				for (int j = 0; j < this.Cols; j++)
				{
					matrix[i][j] = multiplier * this.Get(i, j);
				}
			}
			
			return new RectangleMatrix(matrix);
		}
		public virtual RectangleMatrix Mul(IMatrix secondMatrix)
		{
			if (this.Cols != secondMatrix.Rows)
			{
				throw new ArgumentException();
			}

			int rows = this.Rows;
			int cols = secondMatrix.Cols;
			double[][] resultMatrix = new double[rows][];
			for (int i = 0; i < rows; i++)
			{
				resultMatrix[i] = new double[cols];
			}

			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < cols; j++)
				{
					resultMatrix[i][j] = new RowVector(GetRow(i))
											.Mul(new ColumnVector(secondMatrix.GetCol(j)));
				}
			}
			return new RectangleMatrix(resultMatrix);
		}
		public virtual RectangleMatrix Mul(IVector vector)
		{
			return this.Mul(vector.ToMatrix());
		}

		public static RectangleMatrix operator * (double multiplier, RectangleMatrix matrix)
			=> matrix.Mul(multiplier);
		public static RectangleMatrix operator * (RectangleMatrix matrix, double multiplier)
			=> matrix.Mul(multiplier);
		public static RectangleMatrix operator * (RectangleMatrix matrix0, IMatrix matrix1)
			=> matrix0.Mul(matrix1);
		public static RectangleMatrix operator * (RectangleMatrix matrix, IVector vector)
			=> matrix.Mul(vector);
		public static RectangleMatrix operator - (RectangleMatrix matrix1, RectangleMatrix matrix2)
			=> matrix1.Sub(matrix2);
		public static RectangleMatrix operator + (RectangleMatrix matrix1, RectangleMatrix matrix2)
			=> matrix1.Sum(matrix2);


		protected virtual RectangleMatrix TransposeImplementation()
		{
			int rows = this.Cols;
			int cols = this.Rows;

			double[][] transposedMatrix = new double[rows][];
			for (int i = 0; i < rows; i++)
			{
				transposedMatrix[i] = this.GetCol(i);
			}

			return new RectangleMatrix(transposedMatrix);
		}		
		

		public RectangleMatrix Transpose()
		{
			return TransposeImplementation();
		}


		public virtual double FirstNorm()
		{
			double maxVal = matrix[0][0];
			for (int i = 0; i < Rows; i++)
			{
				double sum = 0;
				for (int j = 0; j < Cols; j++)
				{
					sum += Math.Abs(Get(i, j));
				}
				if (maxVal < sum)
				{
					maxVal = sum;
				}
			}
			return maxVal;
		}

		public virtual double SecondNorm()
		{
			double maxVal = 0;
			for (int i = 0; i < Cols; i++)
			{
				double sum = 0;
				for (int j = 0; j < Rows; j++)
				{
					sum += Math.Abs(Get(j, i));
				}
				if (maxVal < sum)
				{
					maxVal = sum;
				}
			}
			return maxVal;
		}


		public virtual void SwapRows(int row1, int row2)
		{
			if (row1 < 0 || row1 >= Rows || row2 < 0 || row2 >= Rows)
			{
				throw new ArgumentException();
			}

			for (int i = 0; i < Cols; i++)
			{
				double tmp = Get(row1, i);
				Set(row1, i, Get(row2, i));
				Set(row2, i, tmp);
			}
		}

		public virtual void SwapCols(int col1, int col2)
		{
			if (col1 < 0 || col1 >= Cols || col2 < 0 || col2 >= Cols)
			{
				throw new ArgumentException();
			}

			for (int i = 0; i < Rows; i++)
			{
				double tmp = Get(i, col1);
				Set(i, col1, Get(i, col2));
				Set(i, col2, tmp);
			}
		}

		public virtual void MultiplyRow(int row, double multiplier)
		{
			if (row < 0 || row >= Rows || multiplier == 0)
			{
				throw new ArgumentException();
			}
			for (int i = 0; i < Cols; i++)
			{
				Set(row, i, multiplier*(Get(row, i)));
			}
		}

		public virtual void DivideRow(int row, double divider)
		{
			MultiplyRow(row, 1.0/divider);
		}

		public virtual void AddToRow(int row, double[] vector)
		{
			if (vector.Length != Cols)
			{
				throw new ArgumentException();
			}
			for (int i = 0; i < Cols; i++)
			{
				Set(row, i, vector[i]+Get(row, i));
			}
		}

		public virtual void AddToRow(int row, RowVector vector)
		{
			AddToRow(row, vector.ToArray());
		}

		public virtual void DeleteRow(int row)
		{
			if (row < 0 || row >= Rows)
			{
				throw new ArgumentException();
			}
			List<double[]> matrixAsList = new List<double[]>(matrix);
			matrixAsList.RemoveAt(row);
			matrix = matrixAsList.ToArray();
		}

		public virtual void AddToRowMultipliedRow(int rowToMultiply, int rowToAdd, double multiplier)
		{
			RowVector multipliedRow = new RowVector(GetRow(rowToMultiply)).Mul(multiplier);
			AddToRow(rowToAdd, multipliedRow);
		}
	}
}