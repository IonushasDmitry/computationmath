taskMatrix = [
2 1 1.4 0.5;
1 1 0.5 1;
1.4 0.5 2 1.2;
0.5 1 1.2 0.5
]

[U,V]=eig(taskMatrix)

testMatrix = [
2.2 1 0.5 2;
1 1.3 2 1;
0.5 2 0.5 1.6;
2 1 1.6 2
]

[U,V]=eig(testMatrix)
