using System;
using System.Collections.Generic;

namespace ComputationMath.NonlinearEquations
{
    public class ChordsMethod
    {
        private const double eps = 0.01;
        private const double delta = 0.01;
        public double UpperLimit() => 10;
        public double LowerLimit() => 0;
        Func<double, double> CalculateValue;

        public ChordsMethod(Func<double, double> calvVal)
        {
            CalculateValue = calvVal;
        }


        public List<double> FindSignChange() 
        {
            double upperLimit = this.UpperLimit();
            double lowerLimit = this.LowerLimit();
            // double delta = (Math.Abs(upperLimit) + Math.Abs(lowerLimit)) / 100;
            // System.Console.WriteLine($"delta = {delta}");

            double point1 = lowerLimit;
            double point2 = lowerLimit + delta;
            double value1 = 0;
            double value2 = 0;
            int step = 0;

            List<double> points = new List<double>();

            while (point2 < upperLimit) 
            {
                value1 = CalculateValue(point1);
                value2 = CalculateValue(point2);
                if ( (value1 * value2) <= 0 )
                {
                    step++;
                    // System.Console.WriteLine($"{step})");
                    // System.Console.WriteLine($"f({point1}) = {value1}");
                    // System.Console.WriteLine($"f({point2}) = {value2}");
                    points.Add(ChordsAlgorithm(point1, point1 + delta));
                }
                point1 = point2;
                point2 = point2 + delta;
            }

            return points;
        }

        private double ChordsAlgorithm(double left, double right)
        {
            // System.Console.WriteLine("===================================");
            // System.Console.WriteLine($"alg: {left}, {right}");
            // System.Console.ReadLine();

            double x = left;
            double b = right;
            double multiplier = 1;
            if (CalculateValue(left) < 0)
            {
                x = right;
                b = left;
                multiplier = -1;
            }
            while (true)
            {
                double newX = x 
                    - CalculateValue(x)
                    * ( multiplier*(b - x) 
                    / (multiplier*(CalculateValue(b) - CalculateValue(x))) );
                // System.Console.WriteLine($"{newX}");
                // System.Console.ReadLine();
                if (Math.Abs(newX - x) <= eps)
                {
                    return newX;
                }
                x = newX;
            }
        }
    }
}