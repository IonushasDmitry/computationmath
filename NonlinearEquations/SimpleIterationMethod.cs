using System;
using System.Collections.Generic;

namespace ComputationMath.NonlinearEquations
{
    public class SimpleIterationMethod
    {
        private const double eps = 0.01;
        public double UpperLimit() => 10;
        public double LowerLimit() => -10;
        Func<double, double> CalculateValue;
        Func<double, double> CalculateApproximation;

        public SimpleIterationMethod(Func<double, double> calvVal, Func<double, double> calcApproximation)
        {
            CalculateValue = calvVal;
            CalculateApproximation = calcApproximation;
        }


        public List<double> FindSignChange() 
        {
            double upperLimit = this.UpperLimit();
            double lowerLimit = this.LowerLimit();
            double delta = (Math.Abs(upperLimit) + Math.Abs(lowerLimit)) / 1000;
            // System.Console.WriteLine($"delta = {delta}");

            double point1 = lowerLimit;
            double point2 = lowerLimit + delta;
            double value1 = 0;
            double value2 = 0;
            int step = 0;

            List<double> points = new List<double>();

            while (point2 < upperLimit) 
            {
                value1 = CalculateValue(point1);
                value2 = CalculateValue(point2);
                if ( (value1 * value2) <= 0 )
                {
                    step++;
                    System.Console.WriteLine($"{step})");
                    System.Console.WriteLine($"f({point1}) = {value1}");
                    System.Console.WriteLine($"f({point2}) = {value2}");
                    points.Add(Iterate(point1));
                }
                point1 = point2;
                point2 = point2 + delta;
            }

            return points;
        }

        private double Iterate(double x)
        {
            double oldX = x;
            while (true)
            {
                double newX = CalculateApproximation(oldX);
                // System.Console.WriteLine($"newX: {newX}");
                // System.Console.ReadLine();
                if (Math.Abs(newX - oldX) <= eps)
                {
                    return newX;
                }
                oldX = newX;
            }
        }
    }
}