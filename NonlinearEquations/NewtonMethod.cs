using System;
using System.Collections.Generic;

namespace ComputationMath.NonlinearEquations
{
    public class NewtonMethod
    {
        private int[] equationPowers;
        private double[] equationCoeffs;

        public NewtonMethod(double[] coeffs, int[] powers)
        {
            equationCoeffs = coeffs;
            equationPowers = powers;
            // System.Console.WriteLine("\ncoeffs: ");
            // foreach(var x in coeffs)
            // {
            //     System.Console.Write($"{x} ");
            // }
            // System.Console.WriteLine("\npowers: ");
            // foreach(var x in powers)
            // {
            //     System.Console.Write($"{x} ");
            // }
            // System.Console.WriteLine();
        }

        public double UpperLimit() => UpperLimit(equationCoeffs);

        private double UpperLimit(double[] coefArray) 
        {
            double maxModul = 0;
            for (int i = 0; i < coefArray.Length; i++) 
            {
                if (Math.Abs(coefArray[i]) > maxModul) 
                {
                    maxModul = Math.Abs(coefArray[i]); 
                }
            }

            double powerCoef = 0;
            for (int i = 0; i < coefArray.Length; i++) 
            {
                if (coefArray[i] < 0)
                {
                    powerCoef = i;
                    break;
                }
            }
            double underRoot = maxModul / coefArray[0];
            return (1 + Math.Pow(underRoot, (1.0/powerCoef)));
        }

        public double LowerLimit()
        {
            double[] helpArray = new double[equationCoeffs.Length];
            for (int i = 0; i < equationCoeffs.Length; i++) 
            {
                helpArray[i] = Math.Pow(-1, equationPowers[i] % 2) * equationCoeffs[i];
            }
            
            if (helpArray[0] < 0) 
            {
                for (int j = 0; j < helpArray.Length; j++) 
                {
                    helpArray[j] = - helpArray[j];
                }
            }

            return (- this.UpperLimit(helpArray));
        }

        public List<double> FindSignChange() 
        {
            double upperLimit = this.UpperLimit();
            double lowerLimit = this.LowerLimit();
            double delta = (Math.Abs(upperLimit) + Math.Abs(lowerLimit)) / 100;
            // System.Console.WriteLine($"delta = {delta}");

            double point1 = lowerLimit;
            double point2 = lowerLimit + delta;
            double value1 = 0;
            double value2 = 0;
            int step = 0;

            List<double> points = new List<double>();

            while (point2 < upperLimit) 
            {
                value1 = CalculateValue(point1);
                value2 = CalculateValue(point2);
                if ( (value1 * value2) <= 0 )
                {
                    step++;
                    // System.Console.WriteLine($"{step})");
                    // System.Console.WriteLine($"f({point1}) = {value1}");
                    // System.Console.WriteLine($"f({point2}) = {value2}");
                    points.Add(NewtonAlgorithm(point1 + delta / 2));
                }
                point1 = point2;
                point2 = point2 + delta;
            }

            return points;
        }

        private double CalculateValue(double point) 
        {
            double value = 0;
            for (int i = 0; i < equationCoeffs.Length; i++) 
            {
                value = value + equationCoeffs[i] * Math.Pow(point, equationPowers[i]);
            }
            return value;
        }

        private double CalculateDerivative(double point) 
        {
            double value = 0;
            for (int  i = 0; i < equationCoeffs.Length; i++) 
            {
                if (equationPowers[i] != 0) 
                {
                    value = value + equationCoeffs[i] * equationPowers[i] * Math.Pow(point, equationPowers[i] - 1);
                }
            }
            return value;
        }

        private double NewtonAlgorithm(double point) 
        {
            // int count = 1;
            double step = point;
            double nextStep = point - CalculateValue(point) / CalculateDerivative(point);
            double epsilon = Math.Pow(10, -5);
            while (Math.Abs(nextStep - step) > epsilon) 
            {
                // System.Console.WriteLine($"Step № {count}, Root = {nextStep}, F(x) = {CalculateValue(nextStep)}");
                // count++;
                step = nextStep;
                nextStep = step - CalculateValue(step)/CalculateDerivative(step);
            }
            // System.Console.WriteLine($"Step № {count}, Root = {nextStep}, F(x) = {CalculateValue(nextStep)}");

            return nextStep;
        }


    }
}