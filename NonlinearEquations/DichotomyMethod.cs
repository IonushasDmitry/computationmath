using System;
using System.Collections.Generic;

namespace ComputationMath.NonlinearEquations
{
    public class DichotomyMethod
    {
        private const double eps = 0.01;
        Func<double, double> CalculateValue;

        public DichotomyMethod(Func<double, double> calvVal)
        {
            CalculateValue = calvVal;
        }

        public double UpperLimit() => 100;
        public double LowerLimit() => -100;

        public List<double> FindSignChange() 
        {
            double upperLimit = this.UpperLimit();
            double lowerLimit = this.LowerLimit();
            double delta = (Math.Abs(upperLimit) + Math.Abs(lowerLimit)) / 100;
            // System.Console.WriteLine($"delta = {delta}");

            double point1 = lowerLimit;
            double point2 = lowerLimit + delta;
            double value1 = 0;
            double value2 = 0;
            int step = 0;

            List<double> points = new List<double>();

            while (point2 < upperLimit) 
            {
                value1 = CalculateValue(point1);
                value2 = CalculateValue(point2);
                if ( (value1 * value2) <= 0 )
                {
                    step++;
                    // System.Console.WriteLine($"{step})");
                    // System.Console.WriteLine($"f({point1}) = {value1}");
                    // System.Console.WriteLine($"f({point2}) = {value2}");
                    points.Add(DichotomyAlgorithm(point1, point1 + delta));
                }
                point1 = point2;
                point2 = point2 + delta;
            }

            return points;
        }

        private double DichotomyAlgorithm(double left, double right)
        {
            // System.Console.WriteLine("===================================");
            // System.Console.WriteLine($"alg: {left}, {right}");
            double point = left;
            double leftBorder = left;
            double rightBorder = right;
            while (true)
            {
                double x = (leftBorder + rightBorder) / 2;
                double value = CalculateValue(x);
                // System.Console.WriteLine($"{value}: ({leftBorder}, {rightBorder})");
                // System.Console.ReadLine();
                if (Math.Abs(value) <= eps)
                {
                    return x;
                }
                if (Math.Abs(CalculateValue(leftBorder)) < Math.Abs(CalculateValue(rightBorder)))
                {
                    rightBorder = x;
                }
                else
                {
                    leftBorder = x;
                }
            }
        }
    }
}