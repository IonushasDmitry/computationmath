using System;
using System.Collections.Generic;

namespace ComputationMath.NonlinearEquations
{
    public class SecantMethod
    {
        private const double eps = 0.01;
        Func<double, double> CalculateValue;
        private const double delta = 0.01;

        public SecantMethod(Func<double, double> calvVal)
        {
            CalculateValue = calvVal;
        }

        public double UpperLimit() => 100;
        public double LowerLimit() => -100;

        public List<double> FindSignChange() 
        {
            double upperLimit = this.UpperLimit();
            double lowerLimit = this.LowerLimit();
            // double delta = (Math.Abs(upperLimit) + Math.Abs(lowerLimit)) / 100;
            // System.Console.WriteLine($"delta = {delta}");

            double point1 = lowerLimit;
            double point2 = lowerLimit + delta;
            double value1 = 0;
            double value2 = 0;
            int step = 0;

            List<double> points = new List<double>();

            while (point2 < upperLimit) 
            {
                value1 = CalculateValue(point1);
                value2 = CalculateValue(point2);
                if ( (value1 * value2) <= 0 )
                {
                    step++;
                    // System.Console.WriteLine($"{step})");
                    // System.Console.WriteLine($"f({point1}) = {value1}");
                    // System.Console.WriteLine($"f({point2}) = {value2}");
                    points.Add(Secant(point1));
                }
                point1 = point2;
                point2 = point2 + delta;
            }

            return points;
        }

        private double Secant(double point)
        {
            // double point = left;
            // double leftBorder = left;
            // double rightBorder = right;
            double xK_0 = point;
            double xK_1 = point + delta / 100;
            while (true)
            {
                double xK_2 = xK_1 
                    - CalculateValue(xK_1) 
                    * ( (xK_1 - xK_0) / (CalculateValue(xK_1) - CalculateValue(xK_0)) );
                if (Math.Abs(xK_2 - xK_1) <= eps)
                {
                    return xK_2;
                }
                xK_0 = xK_1;
                xK_1 = xK_2;
            }
        }
    }
}