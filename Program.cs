﻿using System;
using System.Linq;
using System.Collections.Generic;
using ComputationMath.NonlinearEquations;

namespace ComputationMath
{
    public class Program
    {
        static void Main(string[] args)
        {
            SecantMethod method = new SecantMethod(
                (x) => Math.Log(x) - 2*x + 6
            );

            var points = method.FindSignChange();
            foreach(var x in points)
            {
                System.Console.WriteLine(x);
            }

            System.Console.WriteLine("===============");

            ChordsMethod chords = new ChordsMethod(
                (x) => Math.Log(x) - 2*x + 6
            );

            points = chords.FindSignChange();
            foreach(var x in points)
            {
                System.Console.WriteLine(x);
            }
        }
    }
}
