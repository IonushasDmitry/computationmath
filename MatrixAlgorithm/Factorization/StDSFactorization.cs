using ComputationMath.Matrix;
using static System.Math;

namespace ComputationMath.MatrixAlgorithm.Factorization
{
    public class StDSFactorization
    {
        private SymmetricMatrix AMatrix;
        private SquareMatrix SMatrix;
        private SquareMatrix DMatrix;

        public StDSFactorization(SymmetricMatrix Matrix)
        {
            this.AMatrix = Matrix;
            int size = Matrix.Size;
            SMatrix = new SquareMatrix(size);
            DMatrix = new SquareMatrix(size);
            Factorize();
        }
        
        public SquareMatrix GetAMatrix() => AMatrix;
        public UpperTriangularMatrix GetSMatrix() => new UpperTriangularMatrix(SMatrix);
        public SquareMatrix GetDMatrix() => DMatrix;

        private void Factorize()
        {
            int size = AMatrix.Size;
            SMatrix.Set(0, 0, Sqrt(AMatrix.Get(0, 0)));
            DMatrix.Set(0, 0, Sign(AMatrix.Get(0, 0)));

            // Set elements of row 0
            for (int i = 1; i < size; i++)
            {
                double value = AMatrix.Get(0, i) / (DMatrix.Get(0, 0) * SMatrix.Get(0, 0));
                SMatrix.Set(0, i, value);
            }

            // Set 0 to all elements beneath main diagonal
            for (int i = 1; i < size; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    SMatrix.Set(i, j, 0);
                }
            }

            for (int i = 1; i < size; i++)
            {
                // Set D[i, i]
                double sum = 0;
                for (int k = 0; k < i; k++)
                {
                    sum += DMatrix.Get(k, k) * Pow(SMatrix.Get(k, i), 2);
                }
                double value = Sign(AMatrix.Get(i, i) - sum);
                DMatrix.Set(i, i, value);

                // Set S[i, i]
                sum = 0;
                value = 0;
                for (int k = 0; k < i; k++)
                {
                    sum += DMatrix.Get(k, k) * Pow(SMatrix.Get(k, i), 2);
                }
                value = Sqrt(Abs(AMatrix.Get(i, i) - sum));
                SMatrix.Set(i, i, value);

                // Set row i in SMatrix
                for (int j = i+1; j < size; j++)
                {
                    sum = 0;
                    for (int k = 0; k < i; k++)
                    {
                        sum += DMatrix.Get(k, k) * SMatrix.Get(k, i) * SMatrix.Get(k, j);
                    }
                    value = (AMatrix.Get(i, j) - sum) / (SMatrix.Get(i, i) * DMatrix.Get(i, i));
                    SMatrix.Set(i, j, value);
                }
            }
        }
    }
}