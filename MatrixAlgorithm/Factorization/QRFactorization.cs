using ComputationMath.Matrix;
using ComputationMath.Vector;
using static System.Math;

namespace ComputationMath.MatrixAlgorithm.Factorization
{
    public class QRFactorization
    {
        private SquareMatrix AMatrix;
        private SquareMatrix QMatrix;
        private SquareMatrix RMatrix;
        public readonly int Size;

        public QRFactorization(SquareMatrix Matrix)
        {
            this.AMatrix = Matrix;
            Size = Matrix.Size;
            Factorize();
        }

        public SquareMatrix GetAMatrix() => AMatrix;
        public SquareMatrix GetQMatrix() => QMatrix;
        public SquareMatrix GetRMatrix() => RMatrix;

        private void Factorize()
        {
            SquareMatrix AMatrixIterated = new SquareMatrix(AMatrix.Clone());
            SquareMatrix[] HMatrixes = new SquareMatrix[Size-1];

            for (int i = 0; i < Size-1; i++)
            {
                double s = 0;
                for (int j = i; j < Size; j++)
                {
                    s += Pow(AMatrixIterated.Get(j, i), 2);
                }
                s = Sqrt(s);

                double mu = Pow(2*s*(s - AMatrixIterated.Get(i, i)), -0.5);

                ColumnVector omega = new ColumnVector(AMatrixIterated.GetCol(i));
                for (int j = 0; j < i; j++)
                {
                    omega.Set(j, 0);
                }
                omega.Set(i, (AMatrixIterated.Get(i, i) - s));
                omega *= mu;
                
                HMatrixes[i] = (new EMatrix(Size).ToMatrix() 
                    - new SquareMatrix(2 * omega * omega.T));
                AMatrixIterated = HMatrixes[i] * AMatrixIterated;
            }

            QMatrix = HMatrixes[0];
            for (int i = 1; i < Size-1; i++)
            {
                QMatrix *= HMatrixes[i];
            }

            RMatrix = HMatrixes[Size-2];
            for (int i = Size-3; i >= 0; i--)
            {
                RMatrix *= HMatrixes[i];
            }
            RMatrix *= AMatrix;
        }
    }
}