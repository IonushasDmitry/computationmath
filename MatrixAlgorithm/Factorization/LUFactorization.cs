using ComputationMath.Matrix;
using ComputationMath.Vector;
using System.Collections.Generic;

namespace ComputationMath.MatrixAlgorithm.Factorization
{
    public class LUFactorization
    {
        private SquareMatrix Matrix;
        private LowerTriangularMatrix LMatrix;
        private UpperTriangularMatrix UMatrix;

        public LUFactorization(SquareMatrix matrix)
        {
            this.Matrix = matrix;
            this.LMatrix = new LowerTriangularMatrix(Matrix.Size);
            this.UMatrix = new UpperTriangularMatrix(Matrix.Size);
            Factorize();
        }

        private void Factorize()
        {
            int size = Matrix.Size;
            for (int i = 0; i < size; i++)
            {
                LMatrix.Set(i, i, 1);
            }

            for (int i = 0; i < size; i++)
            {
                for (int k = 0; k < size; k++)
                {
                    if (k < i)
                    {
                        double sum = 0;
                        for (int j = 0; j < k; j++)
                        {
                            sum += LMatrix.Get(i, j) * UMatrix.Get(j, k);
                        }
                        double value = (Matrix.Get(i, k) - sum) / UMatrix.Get(k, k);

                        LMatrix.Set(i, k, value);
                    }

                    if (i <= k)
                    {
                        double sum = 0;
                        for (int j = 0; j < i; j++)
                        {
                            sum += LMatrix.Get(i, j) * UMatrix.Get(j, k);
                        }
                        double value = Matrix.Get(i, k) - sum;
                        UMatrix.Set(i, k, value);
                    }
                }
            }
        }

        public SquareMatrix InverseMatrix()
        {
            int size = Matrix.Size;
            EMatrix eMatrix = new EMatrix(size);
            List<ColumnVector> yList = new List<ColumnVector>();
            List<ColumnVector> xList = new List<ColumnVector>();
            for(int i = 0; i < size; i++)
            {
                yList.Add(LMatrix.Solve(new ColumnVector(eMatrix.GetCol(i))));
            }
            for (int i = 0; i < size; i++)
            {
                xList.Add(UMatrix.Solve(yList[i]));
            }

            
            double[][] xMatrix = new double[size][];
            for (int i = 0; i < size; i++)
            {
                xMatrix[i] = new double[size];
            }
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    xMatrix[j][i] = xList[i].Get(j);
                }
            }

            return new SquareMatrix(xMatrix);
        }

        public LowerTriangularMatrix GetLMatrix() => (LowerTriangularMatrix)LMatrix.Clone();
        public UpperTriangularMatrix GetUMatrix() => (UpperTriangularMatrix)UMatrix.Clone();
    }
}