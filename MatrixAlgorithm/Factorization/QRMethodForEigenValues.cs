using System;
using System.Collections.Generic;
using System.Numerics;
using ComputationMath.Matrix;
using ComputationMath.MatrixAlgorithm.Factorization;

namespace ComputationMath.MatrixAlgorithm.Factorization
{
    public class QRMethodForEigenValues
    {
        private SquareMatrix AMatrix;
        private SquareMatrix factorizedMatrix;
        private const double eps = 0.001;
        private List<Complex> eigenValues = new List<Complex>();

        public QRMethodForEigenValues(SquareMatrix matrix)
        {
            AMatrix = matrix;
            Calculate();
        }

        public SquareMatrix GetFactorizedMatrix() => factorizedMatrix;
        public List<Complex> GetEigenValues() => eigenValues;

        private void Calculate()
        {
            factorizedMatrix = new SquareMatrix(AMatrix.Clone());

            while (true)
            {
                QRFactorization factorization = new QRFactorization(factorizedMatrix);
                factorizedMatrix = factorization.GetRMatrix() * factorization.GetQMatrix();
                // System.Console.WriteLine($"factorized:\n{factorizedMatrix}\n===================");
                if (GetMaxElementBelowDiagonal(factorizedMatrix) <= eps)
                {
                    System.Console.WriteLine(GetMaxElementBelowDiagonal(factorizedMatrix));
                    break;
                }
            }
            eigenValues.Add(new Complex(factorizedMatrix.Get(0, 0), 0));
            eigenValues.Add(new Complex(factorizedMatrix.Get(1, 1), 0));
            var complexValues = SolveSquareEquation(
                1, 
                -factorizedMatrix.Get(2, 2) - factorizedMatrix.Get(3, 3),
                factorizedMatrix.Get(2, 2) * factorizedMatrix.Get(3, 3) - factorizedMatrix.Get(2, 3) * factorizedMatrix.Get(3, 2));
            eigenValues.AddRange(complexValues);
        }

        private double GetMaxElementBelowDiagonal(SquareMatrix matrix)
        {
            double max = Math.Abs(matrix.Get(1, 0));
            for (int i = 1; i < matrix.Size; i++)
            {
                if (max < Math.Abs(matrix.Get(i, 0)))
                {
                    max = Math.Abs(matrix.Get(i, 0));
                }
            }

            return max;
        }

        private Complex[] SolveSquareEquation(double a, double b, double c)
        {
            double d = b*b - 4*a*c;
            Complex sqrtFromD = Complex.Sqrt(new Complex(d, 0));
            return new Complex[] 
            {
                ((-b + sqrtFromD)/(2*a)),
                ((-b - sqrtFromD)/(2*a))
            };
        }
    }
}