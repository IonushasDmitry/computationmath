using ComputationMath.Matrix;
using ComputationMath.Vector;
using System.Collections.Generic;

namespace ComputationMath.MatrixAlgorithm.Factorization
{
    public class LDUFactorization
    {
        private SquareMatrix Matrix;
        private LowerTriangularMatrix LMatrix;
        private UpperTriangularMatrix UMatrix;
        private SquareMatrix DMatrix;

        public LDUFactorization(SquareMatrix Matrix)
        {
            int size = Matrix.Size;
            this.Matrix = Matrix;
            DMatrix = new SquareMatrix(size);
            Factorize();
        }

        private void Factorize()
        {
            int size = Matrix.Size;
            LUFactorization luDecomposition = new LUFactorization(Matrix);
            LMatrix = luDecomposition.GetLMatrix();
            UMatrix = luDecomposition.GetUMatrix();

            for (int i = 0; i < size; i++)
            {
                DMatrix.Set(i, i, UMatrix.Get(i, i));
            }

            for (int i = 0; i < size; i++)
            {
                for (int j = i+1; j < size; j++)
                {
                    UMatrix.Set(i, j, UMatrix.Get(i, j)/UMatrix.Get(i, i));
                }
                UMatrix.Set(i, i, 1);
            }
        }

        public double GetDeterminant()
        {
            double det = 1;
            for (int i = 0; i < DMatrix.Size; i++)
            {
                det *= DMatrix.Get(i, i);
            }
            return det;
        }

        public LowerTriangularMatrix GetLMatrix() => (LowerTriangularMatrix)LMatrix.Clone();
        public UpperTriangularMatrix GetUMatrix() => (UpperTriangularMatrix)UMatrix.Clone();
        public SquareMatrix GetDMatrix() => (SquareMatrix)DMatrix.Clone();
    }
}