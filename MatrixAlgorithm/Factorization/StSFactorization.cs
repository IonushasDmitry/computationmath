using System;
using ComputationMath.Matrix;
using static System.Math;

namespace ComputationMath.MatrixAlgorithm.Factorization
{
    public class StSFactorization
    {
        private SymmetricMatrix AMatrix;
        private SquareMatrix SMatrix;

        public StSFactorization(SymmetricMatrix Matrix)
        {
            if (!Matrix.IsDiagonallyDominant())
            {
                throw new ArgumentException("Passed matrix isn't diagonally dominant");
            }
            this.AMatrix = Matrix;
            int size = Matrix.Size;
            SMatrix = new SquareMatrix(size);
            Factorize();
        }

        public SquareMatrix GetAMatrix() => AMatrix;
        public UpperTriangularMatrix GetSMatrix() => new UpperTriangularMatrix(SMatrix);

        private void Factorize()
        {
            int size = AMatrix.Size;
            SMatrix.Set(0, 0, Sqrt(AMatrix.Get(0, 0)));

            // Set elements of row 0
            for (int i = 1; i < size; i++)
            {
                double value = AMatrix.Get(0, i) / SMatrix.Get(0, 0);
                SMatrix.Set(0, i, value);
            }

            // Set 0 to all elements beneath main diagonal
            for (int i = 1; i < size; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    SMatrix.Set(i, j, 0);
                }
            }

            // Set elements on and above main diagonal
            for (int i = 1; i < size; i++)
            {
                // Set diagonal element
                double sum = 0;
                for (int k = 0; k < i; k++)
                {
                    sum += Pow(SMatrix.Get(k, i), 2);
                }
                double value = Sqrt(AMatrix.Get(i, i) - sum);
                SMatrix.Set(i, i, value);

                // Set elements of row i
                for (int j = i+1; j < size; j++)
                {
                    sum = 0;
                    value = 0;
                    for (int k = 0; k < i; k++)
                    {
                        sum += SMatrix.Get(k, i) * SMatrix.Get(k, j);
                    }
                    value = (AMatrix.Get(i, j) - sum) / SMatrix.Get(i, i);
                    SMatrix.Set(i, j, value);
                }
            }
        }
    }
}