using System;
using ComputationMath.Matrix;

namespace ComputationMath.MatrixAlgorithm.DirectMethods
{
    public class JacobiMethod
    {
        private SymmetricMatrix aMatrix;
        private SquareMatrix bMatrix;
        private SquareMatrix eigenVectors;
        private const double eps = 0.001;

        public SquareMatrix GetBMatrix() => bMatrix;
        public SquareMatrix GetEigenVectors() => eigenVectors;

        public JacobiMethod(SymmetricMatrix matrix)
        {
            aMatrix = matrix;
            bMatrix = new SquareMatrix(aMatrix.Clone());
            eigenVectors = new SquareMatrix(new EMatrix(matrix.Size));
            Calculate();
        }

        private void Calculate()
        {
            while (GetMaxNonDiagonalElement(bMatrix) > eps)
            {
                int[] coordsOfMaxElementAboveDiagonal = 
                    GetMaxElementAboveDiagonalCoordinates(bMatrix);
                SquareMatrix tMatrix = GetTMatrix(bMatrix, 
                    coordsOfMaxElementAboveDiagonal[0],
                    coordsOfMaxElementAboveDiagonal[1]);
                eigenVectors *= tMatrix;

                bMatrix = tMatrix.Transpose() * bMatrix * tMatrix;
                // System.Console.WriteLine($"tMatrix:\n{tMatrix}");
                // System.Console.WriteLine($"bMatrix:\n{bMatrix}\n================");
            }
        }

        private double GetMaxNonDiagonalElement(SquareMatrix matrix)
        {
            double maxVal = Math.Abs(matrix.Get(0, 1));
            for (int i = 0; i < matrix.Size; i++)
            {
                for (int j = 0; j < matrix.Size; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }
                    if (maxVal < Math.Abs(matrix.Get(i, j)))
                    {
                        maxVal = Math.Abs(matrix.Get(i, j));
                    }
                }
            }
            return maxVal;
        }

        private int[] GetMaxElementAboveDiagonalCoordinates(SquareMatrix matrix)
        {
            double maxVal = Math.Abs(matrix.Get(0, 1));
            int row = 0;
            int col = 1;
            for (int i = 0; i < matrix.Size; i++)
            {
                for (int j = i+1; j < matrix.Size; j++)
                {
                    if (maxVal < Math.Abs(matrix.Get(i, j)))
                    {
                        maxVal = Math.Abs(matrix.Get(i, j));
                        row = i;
                        col = j;
                    }
                }
            }
            return new int[] {row, col};
        }

        private SquareMatrix GetTMatrix(SquareMatrix taskMatrix, int row, int col)
        {
            // System.Console.WriteLine($"row: {row}, col: {col}");
            double zNumerator = Math.Pow(2*taskMatrix.Get(row, col), 2);
            double zDenominator = 
                Math.Pow(taskMatrix.Get(row, row) - taskMatrix.Get(row, col), 2)
                + Math.Pow(2*taskMatrix.Get(row, col), 2);
            double z = Math.Sqrt(1 - zNumerator/zDenominator);
            // System.Console.WriteLine($"z: {z}");

            double eps = 0;
            if (taskMatrix.Get(row, row) == taskMatrix.Get(col, col))
            {
                eps = Math.Sign(taskMatrix.Get(row, col));
            }
            else
            {
                eps = Math.Sign((taskMatrix.Get(row, row) - taskMatrix.Get(col, col)) 
                    / taskMatrix.Get(row, col));
            }
            // System.Console.WriteLine($"eps: {eps}");

            double c = Math.Sqrt((1+z) / 2);
            double s = eps * Math.Sqrt((1 - z) / 2);

            // System.Console.WriteLine($"c: {c}; s: {s}");

            SquareMatrix tMatrix = new SquareMatrix(new EMatrix(taskMatrix.Size));

            tMatrix.Set(row, row, c);
            tMatrix.Set(row, col, -s);
            tMatrix.Set(col, row, s);
            tMatrix.Set(col, col, c);

            return tMatrix;
        }
    }
}