using System;
using ComputationMath.Matrix;
using ComputationMath.Vector;
using ComputationMath.MatrixAlgorithm.Factorization;

namespace ComputationMath.MatrixAlgorithm.DirectMethods.FactorizationBased
{
    public class FactorizationBasedGauss
    {
        private SquareMatrix AMatrix;
        private ColumnVector FVector;

        public FactorizationBasedGauss(SquareMatrix aMatrix, ColumnVector fVector)
        {
            AMatrix = aMatrix;
            FVector = fVector;
        }

        public ColumnVector Solve()
        {
            LUFactorization luFactorization = new LUFactorization(AMatrix);
            LowerTriangularMatrix lMatrix = luFactorization.GetLMatrix();
            UpperTriangularMatrix uMatrix = luFactorization.GetUMatrix();

            ColumnVector g = lMatrix.Solve(FVector);
            return uMatrix.Solve(g);
        }
    }
}