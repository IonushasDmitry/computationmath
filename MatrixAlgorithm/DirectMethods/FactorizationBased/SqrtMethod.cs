using ComputationMath.Matrix;
using ComputationMath.Vector;
using ComputationMath.MatrixAlgorithm.Factorization;

namespace ComputationMath.MatrixAlgorithm.DirectMethods.FactorizationBased
{
    public class SqrtMethod
    {
        private SymmetricMatrix AMatrix;
        private ColumnVector FVector;

        public SqrtMethod(SymmetricMatrix matrix, ColumnVector vector)
        {
            AMatrix = matrix;
            FVector = vector;
        }

        public ColumnVector Solve()
        {
            StDSFactorization stdsFactorization = new StDSFactorization(AMatrix);
            ColumnVector Y 
                = new LowerTriangularMatrix(
                    stdsFactorization.GetSMatrix().Transpose()
                    * stdsFactorization.GetDMatrix())
                .Solve(FVector);
            return stdsFactorization.GetSMatrix().Solve(Y);
        }
    }
}