using System;
using ComputationMath.Matrix;
using ComputationMath.Vector;

namespace ComputationMath.MatrixAlgorithm.DirectMethods
{
    public class GaussJordanMethod
    {
        private SquareMatrix AMatrix;
        private ColumnVector FVector;
        private AugmentMatrix AugmentMatrix;

        public GaussJordanMethod(SquareMatrix aMatrix, ColumnVector fVector)
        {
            AMatrix = aMatrix;
            FVector = fVector;
            AugmentMatrix = new AugmentMatrix(aMatrix, fVector);
        }

        public ColumnVector Solve()
        {
            int size = AugmentMatrix.Rows;
            for (int i = 0; i < size; i++)
            {
                int indexMax = i;
                for (int j = i + 1; j < size; j++)
                {
                    if (Math.Abs(AugmentMatrix.Get(i, j)) > Math.Abs(AugmentMatrix.Get(i, indexMax)))
                    {
                        indexMax = j;
                    }
                }

                AugmentMatrix.SwapRows(i, indexMax);
                AugmentMatrix.DivideRow(i, AugmentMatrix.Get(i, i));

                for (int j = 0; j < size; j++)
                {
                    if (j == i)
                    {
                        continue;
                    }
                    AugmentMatrix.AddToRowMultipliedRow(i, j, -AugmentMatrix.Get(j, i));
                }
            }

            return new ColumnVector(AugmentMatrix.AdditionalMatrix);
        }
    }
}