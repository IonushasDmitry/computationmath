using System;
using ComputationMath.Matrix;
using ComputationMath.Vector;

namespace ComputationMath.MatrixAlgorithm.DirectMethods
{
    public class SweepMethod
    {
        private ThreeDiagonalMatrix Matrix;
        private ColumnVector FVector;

        public SweepMethod(ThreeDiagonalMatrix matrix, ColumnVector vector)
        {
            // TODO: check if passed matrix is diagonally dominant
            // if (!matrix.IsDiagonallyDominant())
            // {
            //     throw new ArgumentException("Passed matrix isn't diagonally dominant");
            // }
            Matrix = matrix;
            FVector = vector;
        }

        public ColumnVector Solve()
        {
            int size = Matrix.Size;
            double[] aCoeffs = new double[size];
            double[] bCoeffs = new double[size];

            aCoeffs[0] = -Matrix.Get(0, 1) / Matrix.Get(0, 0);
            bCoeffs[0] = FVector.Get(0) / Matrix.Get(0, 0);
            for (int i = 1; i < size-1; i++)
            {
                aCoeffs[i] = -Matrix.Get(i, i+1) 
                    / ( Matrix.Get(i, i) + Matrix.Get(i, i-1) * aCoeffs[i-1] );
                bCoeffs[i] = (FVector.Get(i) - Matrix.Get(i, i-1) * bCoeffs[i-1])
                    / (Matrix.Get(i, i) + Matrix.Get(i, i-1) * aCoeffs[i-1]);
            }

            double[] xVector = new double[size];
            xVector[size-1] = (FVector.Get(size-1) - Matrix.Get(size-1, size-2) * bCoeffs[size-2])
                 / (Matrix.Get(size-1, size-1) + Matrix.Get(size-1, size-2) * aCoeffs[size-2]);
            for (int i = size-2; i >= 0; i--)
            {
                xVector[i] = aCoeffs[i] * xVector[i+1] + bCoeffs[i];
            }

            return new ColumnVector(xVector);
        }
    }
}