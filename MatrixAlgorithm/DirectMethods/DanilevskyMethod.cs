using System;
using System.Collections.Generic;
using System.Linq;
using ComputationMath.Matrix;
using ComputationMath.Vector;
using ComputationMath.MatrixAlgorithm;
using ComputationMath.NonlinearEquations;

namespace ComputationMath.MatrixAlgorithm.DirectMethods
{
    public class DanilevskyMethod
    {
        private SquareMatrix AMatrix;
        private SquareMatrix frobeniusMatrix;
        private SquareMatrix sMatrix;
        private List<double> eigenValues;
        private List<ColumnVector> eigenVectors;

        public DanilevskyMethod(SquareMatrix matrix)
        {
            AMatrix = matrix;
            Calculate();
        }

        public SquareMatrix GetFrobeniusMatrix()
        {
            return frobeniusMatrix;
        }
        
        public SquareMatrix GetSMatrix()
        {
            return sMatrix;
        }

        public List<double> GetEigenValues()
        {
            return eigenValues;
        }

        public List<ColumnVector> GetEigenVectors()
        {
            return eigenVectors;
        }

        private void Calculate()
        {
            int size = AMatrix.Size;
            FrobeniusMatrixCreator frobeniusMatrixCreator 
                = new FrobeniusMatrixCreator(AMatrix);
            
            frobeniusMatrix = frobeniusMatrixCreator.GetFrobeniusMatrix();
            sMatrix = frobeniusMatrixCreator.GetSMatrix();

            // System.Console.WriteLine($"frobenius matrix:\n{frobeniusMatrix}\n=================");
            // System.Console.WriteLine($"smatrix:\n{sMatrix}\n=================");


            List<double> eigenEquationCoeffs = new List<double>() {1};
            foreach(var x in frobeniusMatrix.GetRow(0))
            {
                eigenEquationCoeffs.Add(-x);
            }
            NewtonMethod newton = new NewtonMethod(
                eigenEquationCoeffs.ToArray(),
                Enumerable.Range(0, eigenEquationCoeffs.ToArray().Length).Reverse().ToArray()
            );
            eigenValues = newton.FindSignChange();

            List<ColumnVector> eigenVectors = new List<ColumnVector>();
            foreach(var eigenVal in eigenValues)
            {
                double[] yCoeffs = new double[size];
                for (int i = 0; i < size; i++)
                {
                    yCoeffs[i] = Math.Pow(eigenVal, size-i-1);
                }

                // System.Console.WriteLine($"eigenVal: {eigenVal}\nyVector:\n{new ColumnVector(yCoeffs)}");

                eigenVectors.Add(sMatrix * new ColumnVector(yCoeffs));
            }

            this.eigenVectors = eigenVectors;
        }
    }
}