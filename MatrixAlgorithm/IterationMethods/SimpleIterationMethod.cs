using System;
using ComputationMath.Matrix;
using ComputationMath.Vector;

namespace ComputationMath.MatrixAlgorithm.IterationMethods
{
    public class SimpleIterationMethod
    {
        private SquareMatrix AMatrix;
        private ColumnVector FVector;
        private double Eps;

        public SimpleIterationMethod(SquareMatrix matrix, ColumnVector vector, double eps)
        {
            Eps = eps;
            if (!matrix.IsDiagonallyDominant())
            {
                // throw new ArgumentException("Passed matrix isn't diagonally dominant");
                AugmentMatrix augmentMatrix = new AugmentMatrix(matrix, vector);
                for (int i = 0; i < matrix.Size; i++)
                {
                    int maxValIndex = i;
                    for (int j = i+1; j < matrix.Size; j++)
                    {
                        if (augmentMatrix.Get(j, i) > augmentMatrix.Get(maxValIndex, i))
                        {
                            maxValIndex = j;
                        }
                    }
                    augmentMatrix.SwapRows(i, maxValIndex);
                    AMatrix = new SquareMatrix(augmentMatrix.MainMatrix);
                    FVector = new ColumnVector(augmentMatrix.AdditionalMatrix);
                }
            }
            else
            {
                AMatrix = matrix;
                FVector = vector;
            }
        }

        public ColumnVector Solve()
        {
            int size = AMatrix.Size;
            double[] g = new double[size];
            double[][] B = new double[size][];

            for (int i = 0; i < size; i++)
            {
                B[i] = new double[size];
                g[i] = FVector.Get(i) / AMatrix.Get(i, i);
                for (int j = 0; j < size; j++)
                {
                    B[i][j] = -AMatrix.Get(i, j) / AMatrix.Get(i, i);
                }
                B[i][i] = 0;
            }

            ColumnVector gVector = new ColumnVector(g);
            SquareMatrix BMatrix = new SquareMatrix(B);

            if (BMatrix.FirstNorm() >= 1 && BMatrix.SecondNorm() >= 1)
            {
                throw new ArgumentException("First and second norm of BMatrix >= 1");
            }

            ColumnVector xVector = new ColumnVector(gVector.ToArray());

            int numOfIterations = 0;
            do
            {
                numOfIterations++;
                ColumnVector newXVector = new ColumnVector(size);
                for (int i = 0; i < size; i++)
                {
                    double currentX = 0;
                    for (int j = 0; j < size; j++)
                    {
                        currentX += BMatrix.Get(i, j) * xVector.Get(j);
                    }
                    currentX += gVector.Get(i);
                    newXVector.Set(i, currentX);
                }

                double maxVal = 0;
                for (int i = 0; i < size; i++)
                {
                    if (Math.Abs(newXVector.Get(i) - xVector.Get(i)) > maxVal)
                    {
                        maxVal = Math.Abs(newXVector.Get(i) - xVector.Get(i));
                    }
                }
                
                xVector = newXVector;
                if (maxVal < Eps)
                {
                    break;
                }
            } while (true);

            System.Console.WriteLine($"number of iterations: {numOfIterations}");
            return xVector;
        }
    }
}