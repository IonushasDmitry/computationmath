using System;
using System.Linq;
using ComputationMath.Matrix;
using ComputationMath.Vector;

namespace ComputationMath.MatrixAlgorithm.IterationMethods
{
    public class IterMethodForMaxEigenValue
    {
        private SquareMatrix AMatrix;
        private const double eps = 0.001;
        private double maxEigenValue;
        private ColumnVector eigenVector;

        public double GetMaxEigenValue() => maxEigenValue;
        public ColumnVector GetEigenVector() => eigenVector;
        
        public IterMethodForMaxEigenValue(SquareMatrix matrix)
        {
            AMatrix = matrix;
            Calculate();
        }

        private void Calculate()
        {
            int size = AMatrix.Size;
            // double maxDelta = eps + 1;
            double oldLambda = Double.MaxValue;
            ColumnVector oldY = new ColumnVector(Enumerable.Repeat(1.0, size).ToArray());
            while(true)
            {
                ColumnVector newY = AMatrix * oldY;
                double newLambda = newY.Get(0) / oldY.Get(0);
                double maxDelta = Math.Abs(newLambda - oldLambda);
                oldY = newY;
                oldLambda = newLambda;
                System.Console.WriteLine($"lambda: {newLambda}");
                System.Console.WriteLine($"y:\n{newY}");
                System.Console.WriteLine("-------------------");
                if (maxDelta <= eps)
                {
                    break;
                }
            }

            maxEigenValue = oldLambda;
            eigenVector = oldY * (1/oldY.Get(oldY.Size-1));
        }
    }
}