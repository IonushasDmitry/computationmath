using System;
using ComputationMath.Matrix;
using ComputationMath.Vector;

namespace ComputationMath.MatrixAlgorithm.IterationMethods
{
    public class FastestDescendMethod
    {
        private SymmetricMatrix AMatrix;
        private ColumnVector FVector;
        private double Eps;

        public FastestDescendMethod(SymmetricMatrix matrix, ColumnVector vector, double eps)
        {
            AMatrix = matrix;
            FVector = vector;
            Eps = eps;
        }

        public ColumnVector Solve()
        {
            int size = FVector.Size;
            ColumnVector xVector = new ColumnVector(size);

            while (true)
            {
                ColumnVector rVector = new ColumnVector(AMatrix * xVector) - FVector;
                double t = (rVector.T * rVector) / (new ColumnVector(AMatrix*rVector).T * rVector);
                ColumnVector newXVector = xVector - t*rVector;
                double maxDelta = 0;
                for (int i = 0; i < size; i++)
                {
                    if (maxDelta < Math.Abs(xVector.Get(i) - newXVector.Get(i)))
                    {
                        maxDelta = Math.Abs(xVector.Get(i) - newXVector.Get(i));
                    }
                }
                xVector = newXVector;
                if (maxDelta <= Eps)
                {
                    break;
                }
            }

            return xVector;
        }
    }
}