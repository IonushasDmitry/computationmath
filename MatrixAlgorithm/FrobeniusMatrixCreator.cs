using System;
using ComputationMath.Matrix;

namespace ComputationMath.MatrixAlgorithm
{
    public class FrobeniusMatrixCreator
    {
        private SquareMatrix AMatrix;
        private SquareMatrix FrobeniusMatrix;
        private SquareMatrix SMatrix;

        public SquareMatrix GetFrobeniusMatrix() => FrobeniusMatrix;
        public SquareMatrix GetSMatrix() => SMatrix;
        
        public FrobeniusMatrixCreator(SquareMatrix matrix)
        {
            AMatrix = matrix;
            CreateFrobeniusMatrix();
        }

        private void CreateFrobeniusMatrix()
        {
            int size = AMatrix.Size;
            SquareMatrix aMatrix = new SquareMatrix(AMatrix.Clone());
            SMatrix = new EMatrix(size).ToMatrix();

            for (int i = size-2; i >= 0; i--)
            {
                SquareMatrix mMatrix = new SquareMatrix(new EMatrix(size));
                SquareMatrix inverseMMatrix = new SquareMatrix(new EMatrix(size));

                for (int j = 0; j < size; j++)
                {
                    mMatrix.Set(i, j, (-aMatrix.Get(i+1, j) / aMatrix.Get(i+1, i)));
                    inverseMMatrix.Set(i, j, aMatrix.Get(i+1, j));
                }
                mMatrix.Set(i, i, (1 / aMatrix.Get(i+1, i)));

                // System.Console.WriteLine($"M{i}:\n{mMatrix}");
                SMatrix *= mMatrix;

                aMatrix = inverseMMatrix * aMatrix * mMatrix;
            }

            FrobeniusMatrix = aMatrix;
        }
    }
}